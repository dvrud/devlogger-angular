import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { Observable } from "rxjs";
import { of } from "rxjs";

import { Log } from "../models/logs";
@Injectable({
  providedIn: "root"
})
export class LogService {
  // the defined logs are gonna go to the log component and it wiill fetch the data there through this service.
  logs: Log[];
  // When using behaviour subject assign the type and initial value basically null values of type
  // behaviour subject basically is a class which watches for any event in the component like click and takes action as per the method....like below the behaviour subject defined is assigned to observable and will be used when any entry of log is clicked and we want it in form.
  private logSource = new BehaviorSubject<Log>({
    id: null,
    text: null,
    date: null
  });

  selectedLog = this.logSource.asObservable();

  private stateSource = new BehaviorSubject<boolean>(true);

  stateClear = this.stateSource.asObservable();
  constructor() {
    // this.logs = [
    //   { id: "1", text: "Generated Components", date: new Date() },
    //   { id: "2", text: "Generated Components 2", date: new Date() },
    //   { id: "3", text: "Generated Components 3", date: new Date() }
    // ];

    this.logs = [];
  }

  getLogs(): Observable<Log[]> {
    if (localStorage.getItem("logs") === null) {
      this.logs = [];
    } else {
      this.logs = JSON.parse(localStorage.getItem("logs"));
    }
    return of(
      this.logs.sort((a, b) => {
        return b.date - a.date;
      })
    );
  }

  setFormLog(log: Log) {
    this.logSource.next(log);
  }

  addLog(log: Log) {
    this.logs.unshift(log);

    // Storing in the local storage
    localStorage.setItem("logs", JSON.stringify(this.logs));
  }

  updateLog(log: Log) {
    this.logs.forEach((cur, index) => {
      if (log.id == cur.id) {
        this.logs.splice(index, 1);
      }
    });
    this.logs.unshift(log);

    // Updating in the local storage
    localStorage.setItem("logs", JSON.stringify(this.logs));
  }

  deleteLog(log) {
    this.logs.forEach((cur, index) => {
      if (log.id == cur.id) {
        this.logs.splice(index, 1);
      }
    });

    // Deleting in the local storage
    localStorage.setItem("logs", JSON.stringify(this.logs));
  }

  clearState() {
    this.stateSource.next(true);
  }
}
